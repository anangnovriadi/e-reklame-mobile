import React, { Component } from "react";
import { View, Text, Image, TextInput, TouchableOpacity, Alert, Button, StyleSheet, StatusBar} from 'react-native';
import { createStackNavigator, createAppContainer } from 'react-navigation';
import Dashboard from './App';

class App extends Component {
    static navigationOptions = {
        title: "Welcome to E-Reklame"
    }

    constructor(props) {
        super(props);
        
        this.state = {
            emailText: '',
            passText: ''
        }
    }

    loginAction = () => {
        const { emailText, passText } = this.state;
        // alert(emailText)

        fetch("http://ereklame.cktr.web.id/webservice/ws_login.php?u="+ emailText +"&p="+ passText +"", {
            method: 'post',
            header: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: {}
        })
        .then((response) => response.json())
        .then((responseJson) => {
            if(responseJson.sukses == 1) {
                this.props.navigation.navigate("Dashboard");
            } else {
                alert('User dan Password Salah!')
            }
            console.log(responseJson)
        })
        .catch((err) => {
            console.log(err)
        })
    }

    render() {
        return(
            <View style={stylesContainer.container}>
                <View style={stylesContainer.loginContainer}>
                    <Image resizeMode="contain" style={stylesContainer.logo} source={require('../assets/img/logo-dark-bg.png')} />
                </View>
                <View style={stylesContainer.formContainer}>
                    <View style={styles.container}>
                        <StatusBar barStyle="light-content"/>
                        <TextInput 
                            style = {styles.input} 
                            autoCapitalize="none" 
                            onSubmitEditing={() => this.passwordInput.focus()} 
                            autoCorrect={false} 
                            keyboardType='email-address' 
                            returnKeyType="next" 
                            placeholder='Email or Mobile Num' 
                            placeholderTextColor='rgba(225,225,225,0.7)'
                            onChangeText={emailText => this.setState({emailText})}
                        />

                        <TextInput 
                            style = {styles.input}   
                            returnKeyType="go" ref={(input)=> this.passwordInput = input} 
                            placeholder='Password' 
                            placeholderTextColor='rgba(225,225,225,0.7)' 
                            onChangeText={passText => this.setState({passText})}
                            secureTextEntry
                        />
                        <TouchableOpacity style={styles.buttonContainer} onPress={this.loginAction}>
                            <Text style={styles.buttonText}>LOGIN</Text>
                        </TouchableOpacity> 
                    </View>
                </View>
            </View>
        )
    }
}

const stylesContainer = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#3a3134'
    },
    loginContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        flexGrow: 1
    },
    logo: {
        position: 'absolute',
        width: 240,
        height: 80
    },
    title: {
        color: "#FFF",
        marginTop: 120,
        width: 180,
        textAlign: 'center',
        opacity: 0.9
    }
})

const styles = StyleSheet.create({
    container: {
        padding: 35
    },
    input:{
        height: 40,
        backgroundColor: 'rgba(225,225,225,0.2)',
        marginBottom: 10,
        padding: 10,
        color: '#fff'
    },
    buttonContainer:{
        backgroundColor: '#2980b6',
        paddingVertical: 15
    },
    buttonText:{
        color: '#fff',
        textAlign: 'center',
        fontWeight: '700'
    }, 
    loginButton:{
      backgroundColor:  '#2980b6',
       color: '#fff'
    }
});

const Navigation = createStackNavigator({
    Home: {
        screen: App
    },
    Dashboard: {
        screen: Dashboard
    }
})

export default createAppContainer(Navigation);