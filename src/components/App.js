import React, { Component } from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';

class App extends Component {
    goToLogin = () => {
       this.props.navigation.navigate("Login");
    }

    render() {
        return (
           <View>
               <Text>Hello!</Text>
               <Button onPress={this.goToLogin} title="Go To" />
           </View>
        );
    }
}

export default App;